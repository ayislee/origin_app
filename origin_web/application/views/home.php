<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Freshly Baked | By Origin Bakery</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="utf-8">
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/images/bread.ico"/>
		<!-- bootstrap-css -->
		<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<!--// bootstrap-css -->
		<!-- css -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css" type="text/css" media="all" />
		<!--// css -->
		<!-- font-awesome icons -->
		<link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet">
		<!-- //font-awesome icons -->
		<!-- font -->
		<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<!-- //font -->
		<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
						$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<!-- animation -->
		<link href="<?php echo base_url()?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="<?php echo base_url()?>assets/js/wow.min.js"></script>
		<script>
			new WOW().init();
		</script>
		<!-- //animation -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- banner -->
		<div class="banner" id="banner">
			<div class="container">
				<div class="header-top-grids">
					<div class="header">
						<div class="header-left">
							<div class="w3layouts-logo">
								<a href="index.php"><img src="<?php echo base_url()?>assets/images/logo.png" alt="" width="200" /></a>
							</div>
						</div>
					</div>
					<div class="top-nav">
						<div class="top-nav-info">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									</button>
								</div>
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav" id="mmenu">
										<li><a href="#banner">Home</a></li>
										<li><a href="#serve">Menu</a></li>
										<li><a href="#residence" id="resPro">Residence Programs</a></li>
										<li><a href="#lounge">Lounge</a></li>
										<li><a href="#contact">Contact</a></li>
									</ul>
									<div class="clearfix"> </div>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="holder">
			<div class="ellipse ellipse1"></div>
			<div class="ellipse ellipse2"></div>
		</div>
		<!-- //banner -->
		<!-- welcome -->
		<section class="welcome" id="welcome">
			<div class="welcome_container">
				<div class="welcomeMsg">
					<center><h1 class="welcomeh1"><b>Hello,</b> welcome!</h1></center>
				</div>
				<div class="container">
					<div class="col-sm-12 paddingcol">
						<div id="slider">
							<div class="dp-wrap">
								<div id="dp-slider">
									<div class="dp_item" data-class="1" data-position="1">
										<div class="dp-img">
											<img class="img-fluid" src="<?php echo base_url()?>assets/images/2018-09-17 03.26.29 1.jpg" alt="">
										</div>
									</div>
									<div class="dp_item" data-class="2" data-position="2">
										<div class="dp-img">
											<img class="img-fluid" src="<?php echo base_url()?>assets/images/2018-09-17 03.27.45 1.jpg" alt="">
										</div>
									</div>
								</div>
								
								<span id="dp-next" style="display: none;">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.401 51.401">
										<defs>
										<style>
										.cls-1 {
										fill: none;
										stroke: #fa8c8c;
										stroke-miterlimit: 10;
										stroke-width: 7px;
										}
										</style>
										</defs>
										<path id="Rectangle_4_Copy" data-name="Rectangle 4 Copy" class="cls-1" d="M32.246,0V33.178L0,31.953" transform="translate(0.094 25.276) rotate(-45)"/>
									</svg>
								</span>
								<span id="dp-prev" style="display: none;">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.401 51.401">
										<defs>
										<style>
										.cls-1 {
										fill: none;
										stroke: #fa8c8c;
										stroke-miterlimit: 10;
										stroke-width: 7px;
										}
										</style>
										</defs>
										<path id="Rectangle_4_Copy" data-name="Rectangle 4 Copy" class="cls-1" d="M32.246,0V33.178L0,31.953" transform="translate(0.094 25.276) rotate(-45)"/>
									</svg>
								</span>
								<ul id="dp-dots" style="display: none;">
									<li data-class="1" class="active"></li>
									<li data-class="2"></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="holder" style="margin-top:80px;">
				<div class="ellipse ellipse1"></div>
				<div class="ellipse ellipse2"></div>
			</div>
		</section>
		<!-- //welcome -->
		<!-- about us -->
		<section class="about" id="about">
			<div class="about_container">
				<div class="col-md-6 wow fadeInLeft" data-wow-duration="3s">
					<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000">
						<div class="carousel-inner">
							<div class="item active">
								<img src="<?php echo base_url()?>assets/images/2018-09-17 03.34.03 1.jpg" style="width:100%;">
							</div>
							<div class="item">
								<img src="<?php echo base_url()?>assets/images/2018-09-17 03.39.43 1.jpg" style="width:100%;">
							</div>
							<div class="item">
								<img src="<?php echo base_url()?>assets/images/2018-09-18 03.55.11 1.jpg" style="width:100%;">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 banner_bottom_left wow fadeInUp" data-wow-duration="3s">
					<h3 class="about-us">About us</h3>
					<p> Established in 2017, Origin Bakery is a start-up retail wholesale bakery, located in Gading Serpong, Tangerang. Our European “Healthy Bread” concept serve the need of healthy food with low calories. Origin Bakery aims and commited to deliver the freshest healthy artisan bread.</p><br>
					<p>Under this industry experience due to our partner Freshly Baked by Le Bijoux , we able to maintain the consistency of the products quality. The rapid growth and demand of our product, Origin Bakery wants to establish a large customer base around the country. Therefore, the consistency of our competitive products and service are the main priority.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</section>
		<!-- //about us -->
		<div class="holder">
			<div class="ellipse ellipse1"></div>
			<div class="ellipse ellipse2"></div>
		</div>
		<!-- what we serve -->
		<section class="serve" id="serve">
			<div class="serve_container">
				<div class="wow fadeInUp" data-wow-duration="3s">
					<center><h3 class="what-heading">What we serve</h3>
					<p class="what-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat vo.</p></center>
				</div>
				<div class="agile_team_grids wow fadeInDown" data-wow-duration="3s">
					<div class="col-md-3">
						<div class="view w3-agile-view">
							<img src="<?php echo base_url()?>assets/images/photo_2019-04-30_16-37-13.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask">
								<h5>Bread</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="social">
									<ul>
										<li><a href="#"><button class="btn btn-danger">See More</button></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view w3-agile-view">
							<img src="<?php echo base_url()?>assets/images/2019-04-30 06.05.45 1.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask">
								<h5>Sandwich</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="social">
									<ul>
										<li><a href="#"><button class="btn btn-danger">See More</button></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view w3-agile-view">
							<img src="<?php echo base_url()?>assets/images/photo_2019-04-30_16-33-58.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask">
								<h5>Cookies and Cake</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="social">
									<ul>
										<li><a href="#"><button class="btn btn-danger">See More</button></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 agile_team_grid">
						<div class="view w3-agile-view">
							<img src="<?php echo base_url()?>assets/images/2018-09-24 02.19.40 1.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask">
								<h5>Beverages</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="social">
									<ul>
										<li><a href="#"><button class="btn btn-danger">See More</button></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</section>
		<!-- //what we serve -->
		<div class="holder">
			<div class="ellipse ellipse1"></div>
			<div class="ellipse ellipse2"></div>
		</div>
		<!-- Residences Program -->
		<section class="residence" id="residence">
			<div class="residence_container">
				<div class="col-md-6 banner_bottom_left wow fadeInUp" data-wow-duration="3s">
					<h3 class="resprog">Residences Program</h3>
					<p> Established in 2017, Origin Bakery is a start-up retail wholesale bakery, located in Gading Serpong, Tangerang. Our European “Healthy Bread” concept serve the need of healthy food with low calories. Origin Bakery aims and commited to deliver the freshest healthy artisan bread.</p><br>
					<p>Under this industry experience due to our partner Freshly Baked by Le Bijoux , we able to maintain the consistency of the products quality.</p>
					<button class="btn btn-danger join-now"> Join Now!</button>
				</div>
				<div class="col-md-6 wow fadeInRight" data-wow-duration="3s">
					<div id="myCarousel2" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
						<div class="carousel-inner">
							<div class="item active">
								<img src="<?php echo base_url()?>assets/images/IGPOST-VALENTINE.jpg" style="width:100%;">
							</div>
							<div class="item">
								<img src="<?php echo base_url()?>assets/images/igpost2.jpg" style="width:100%;">
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</section>
		<!-- //Residences Program -->
		<div class="holder">
			<div class="ellipse ellipse1"></div>
			<div class="ellipse ellipse2"></div>
		</div>
		<!-- Lounge -->
		<section class="lounge" id="lounge">
			<div class="lounge_container">
				<div class="wow fadeInUp" data-wow-duration="3s">
					<center><h3 class="what-heading">Lounge</h3>
					<p class="what-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p></center>
				</div>
				<div class="agile_team_grids wow fadeInDown" data-wow-duration="3s">
					<div class="col-md-4 agile_team_grid2">
						<div class="view2 w3-agile-view2">
							<img src="<?php echo base_url()?>assets/images/2018-09-17 03.27.45 1.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask2">
								<h5>How To Increase<br>Your Fiber Intake</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="readMore">
									<a href="#"><button class="btn rd btn-default">Read more</button></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 agile_team_grid2">
						<div class="view2 w3-agile-view2">
							<img src="<?php echo base_url()?>assets/images/2018-09-17 03.39.43 1.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask2">
								<h5>Simple & Delicious<br>High Fiber Diet Menu</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="readMore">
									<a href="#"><button class="btn rd btn-default">Read more</button></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 agile_team_grid2">
						<div class="view2 w3-agile-view2">
							<img src="<?php echo base_url()?>assets/images/2018-09-18 03.55.11 1.jpg" alt=" " class="img-responsive" />
							<div class="w3lmask2">
								<h5>Get To Know About<br>‘High Fiber Diet’</h5>
								<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
								<div class="readMore">
									<a href="#"><button class="btn rd btn-default">Read more</button></a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</section>
		<!-- //Lounge -->
		<div class="holder">
			<div class="ellipse ellipse1"></div>
			<div class="ellipse ellipse2"></div>
		</div>
		<!-- instagram -->
		<section class="instagram" id="instagram">
			<div class="instagram_container">
				<div class="wow fadeInUp" data-wow-duration="3s">
					<center><a href="https://www.instagram.com/freshlybakedbyob/"><h3 class="what-heading">@freshlybakedbyob</h3></a></center>
				</div>
				<div class="row wow fadeInDown" data-wow-duration="3s">
					<div class="col-md-12">
						<!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/59d4f86981615010b2646635becc103b.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
					</div>
				</div>
			</div>
		</section>
		<!-- //instagram -->
		<!-- contact -->
		<section class="contact" id="contact">
			<div class="contact_container">
				<h2 class="contact-desc wow fadeInUp" data-wow-duration="3s">Don’t be a stranger<br>just say <i>hello.</i></h2>
				<div class="col-md-6 contact-p">
					<p class="contact-help">Feel free to get in touch with us. We are always open to answer your questions.</p>
					<p class="email-help">Need Help?<br><b>marketing@originbakery.id</b></p>
					<p class="telp-help">Feel like talking?<br><b>(021) 22228237<br>(WA) 082299800998</b></p>
					<div class="social">
						<ul id="msocial">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 contact-form wow fadeInDown" data-wow-duration="3s">
					<form action="#" method="post">
						<input type="text" id="form-name" name="Name" placeholder="Name" required="">
						<input type="text" id="form-email" name="Number" placeholder="Email" required="">
						<textarea id="form-message" name="Message" placeholder="Message" required=""></textarea>
						<input type="button" id="btn-send" value="Send over" class="btn-submit-form">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</section>
		<!-- //contact -->
		<!-- address -->
		<section class="address" id="address">
			<div class="address_container">
				<div class="col-md-5 address-p wow fadeInUp" data-wow-duration="3s">
					<h3 class="address-desc">Come visit us!</h3>
					<p class="address-text">Ruko Crystal 1 No.17<br>Gading Serpong, Tangerang,<br>Banten 15810<br>Monday — Sunday<br>8 AM — 8 PM</p>
				</div>
				<div class="col-md-7 map wow fadeInRight" data-wow-duration="3s">
					<!-- map -->
					<iframe width="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=origin%20bakery&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					<!-- //map -->
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</section>
		<!-- //address -->
		<div class="footer_container">
			<div class="w3agile_footer_copy">
				<div class="row">
					<a href="index.php"><img src="<?php echo base_url()?>assets/images/logo.png" width="80" /></a>
					<span>&copy; <?php echo date("Y"); ?> Freshly Baked by Origin Bakery. All rights reserved.</span>
				</div>
			</div>
		</div>
	</body>
	<script src="<?php echo base_url()?>assets/js/responsiveslides.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jarallax.js"></script>
	<script src="<?php echo base_url()?>assets/js/circleType.min.js"></script>

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		});

		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	</script>
	<script type="text/javascript" src="js/easing.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
		<script src="<?php echo base_url()?>assets/js/wow.min.js"></script>
		<script>
			new WOW().init();
		</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
		    function detect_active() {
		        // get active
		        var get_active = $("#dp-slider .dp_item:first-child").data("class");
		        $("#dp-dots li").removeClass("active");
		        $("#dp-dots li[data-class=" + get_active + "]").addClass("active");
		    }
		    $("#dp-next").click(function() {
		        var total = $(".dp_item").length;
		        $("#dp-slider .dp_item:first-child").hide().appendTo("#dp-slider").fadeIn();
		        $.each($('.dp_item'), function(index, dp_item) {
		            $(dp_item).attr('data-position', index + 1);
		        });
		        detect_active();
		    });
		    $("#dp-prev").click(function() {
		        var total = $(".dp_item").length;
		        $("#dp-slider .dp_item:last-child").hide().prependTo("#dp-slider").fadeIn();
		        $.each($('.dp_item'), function(index, dp_item) {
		            $(dp_item).attr('data-position', index + 1);
		        });
		        detect_active();
		    });
		    $("#dp-dots li").click(function() {
		        $("#dp-dots li").removeClass("active");
		        $(this).addClass("active");
		        var get_slide = $(this).attr('data-class');
		        $("#dp-slider .dp_item[data-class=" + get_slide + "]").hide().prependTo("#dp-slider").fadeIn();
		        $.each($('.dp_item'), function(index, dp_item) {
		            $(dp_item).attr('data-position', index + 1);
		        });
    		});
		    $setInt = setInterval(function() {
		        $("#dp-next").click();
		    }, 5000);
		    $("body").on("click", "#dp-slider .dp_item:not(:first-child)", function() {
		        var get_slide = $(this).attr('data-class');
		        $("#dp-slider .dp_item[data-class=" + get_slide + "]").hide().prependTo("#dp-slider").fadeIn();
		        $.each($('.dp_item'), function(index, dp_item) {
		            $(dp_item).attr('data-position', index + 1);
		        });
		        detect_active();
		    });

		    $(document).scroll(function() {
		        if ($(document).scrollTop() >= 50) {
		            $("#resPro").text("Residences");
		            $("#mmenu").addClass("smenu");
		        } else {
		            $("#resPro").text("Residences Program");
		            $("#mmenu").removeClass("smenu");
		        }
		        if ($(document).scrollTop() <= 3667 && $(document).scrollTop() >= 50) {
		        	$("#msocial").addClass("ssmenu");
		        } else{
		            $("#msocial").removeClass("ssmenu");
		        }
		    });

		    
    
		});

		$('#btn-send').click(function(){
			send_message();
		});

		function send_message(){
            json_message = '{"message_name": "'+document.getElementById("form-name").value +'",'; 
            json_message += '"message_email": "'+document.getElementById("form-email").value +'",';  
            json_message += '"message_content": "'+document.getElementById("form-message").value +'"}'; 
            
            $.ajax({
                type: "POST",  
                    url: "<?php echo base_url()?>index.php/home/messages",  
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                    	json: json_message,
                        sess: "<?php echo session_id()?>"
                    },
                    dataType: "text",
                    beforeSend: function(){
                    	$("#btn-send").prop("disabled", true);
                    },
                    complete: function(){
                    	$("#btn-send").prop("disabled", false);
                    },
                    success: function(data){
                    	if(data == 'Thanks, We will respond immediately to your message'){
                    		swal("Success", data, "success");
                    	}else{
                    		swal("Warning", data, "warning");
                    	}
                        
                    }
            });

            //alert(json_message);

        }
	</script>
	<!-- //here ends scrolling icon -->
</html>