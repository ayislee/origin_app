<?php
class M_Data extends CI_Model{
	public function save_message($json){
	$obj = json_decode($json);

		$data1 = array(
		   'name' => $obj->message_name ,
		   'email' => $obj->message_email,
		   'message' => $obj->message_content,
		   'ip' => $_SERVER['REMOTE_ADDR'],
		   'datetime'=> date("Y-m-d H:i:s")
		);
		$this->db->insert('messages', $data1);

	}
	
}